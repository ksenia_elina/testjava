package com.example.sweater.domain;

import javax.persistence.*;

@Entity
@Table(name="usr")
public class User {
	@Id
	@GeneratedValue()
	private Long id;
	private String username;
	private String password;
	private boolean active;
	
}
